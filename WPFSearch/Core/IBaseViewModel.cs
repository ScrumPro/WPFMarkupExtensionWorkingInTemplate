﻿using System.Collections.ObjectModel;
using WPFSearch.Helpers;

namespace WPFSearch.Core
{
    public interface IElementSave
    {
        ObservableCollection<ElementPreferenceSetting> ElementPreferenceSettings { get; set; }
    }

    public interface IBaseViewModel
    {
        string ViewName { get; }
    }
}
