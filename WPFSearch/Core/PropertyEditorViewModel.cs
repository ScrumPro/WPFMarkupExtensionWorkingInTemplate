﻿using System.Collections.Generic;
using WPFSearch.Helpers;

namespace WPFSearch.Core
{
    public class PropertyEditorViewModel : NotificationObject
    {
        private ElementSettingProperty _elementSettingProperty;

        public ElementSettingProperty ElementSettingProperty
        {
            get { return _elementSettingProperty; }
            protected set { SetProperty(ref _elementSettingProperty, value); }
        }

        public void InitElementSettingProperties(Dictionary<ElementSettingPropertyName, object> properties)
        {
            ElementSettingProperty = new ElementSettingProperty();

            foreach (var property in properties)
            {
                ElementSettingProperty.Add(property.Key.ToString(), property.Value);
            }
        }
    }
}
