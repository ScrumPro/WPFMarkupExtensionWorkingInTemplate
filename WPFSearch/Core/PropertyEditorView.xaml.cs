﻿using System.Windows;

namespace WPFSearch.Core
{
    /// <summary>
    /// Interaction logic for PropertyEditorView.xaml
    /// </summary>
    public partial class PropertyEditorView : Window
    {
        public PropertyEditorView()
        {
            InitializeComponent();

            DataContext = new PropertyEditorViewModel();
        }
    }
}
