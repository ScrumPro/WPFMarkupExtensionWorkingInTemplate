﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace WPFSearch.Core
{
    public enum ElementSettingPropertyName { Height, Width, GridRowHeight, GridColWidth, Foreground, Background }

    public class ElementSettingProperty : Dictionary<string, object>, ICustomTypeDescriptor
    {
        public AttributeCollection GetAttributes()
        {
            return TypeDescriptor.GetAttributes(this, true);
        }

        public string GetClassName()
        {
            return TypeDescriptor.GetClassName(this, true);
        }

        public string GetComponentName()
        {
            return TypeDescriptor.GetComponentName(this, true);
        }

        public TypeConverter GetConverter()
        {
            return TypeDescriptor.GetConverter(this, true);
        }

        public EventDescriptor GetDefaultEvent()
        {
            return TypeDescriptor.GetDefaultEvent(this, true);
        }

        public PropertyDescriptor GetDefaultProperty()
        {
            return TypeDescriptor.GetDefaultProperty(this, true);
        }

        public object GetEditor(Type editorBaseType)
        {
            return TypeDescriptor.GetEditor(this, editorBaseType, true);
        }

        public EventDescriptorCollection GetEvents()
        {
            return TypeDescriptor.GetEvents(this, true);
        }

        public EventDescriptorCollection GetEvents(Attribute[] attributes)
        {
            return TypeDescriptor.GetEvents(attributes, true);
        }

        public PropertyDescriptorCollection GetProperties()
        {
            throw new NotImplementedException();
        }

        public PropertyDescriptorCollection GetProperties(Attribute[] attributes)
        {
            var properties = new List<ElementPropertyDescriptor>();
            
            foreach (var key in Keys)
            {
                properties.Add(new ElementPropertyDescriptor(key, this));
            }

            return new PropertyDescriptorCollection(properties.ToArray());
        }

        public object GetPropertyOwner(PropertyDescriptor pd)
        {
            return this;
        }
    }

    public class ElementPropertyDescriptor : PropertyDescriptor
    {
        protected readonly ElementSettingProperty Parent;

        public ElementPropertyDescriptor(string propertyName, ElementSettingProperty parent)
            : base(propertyName, null)
        {
            Parent = parent;
        }

        public override bool CanResetValue(object component)
        {
            return true;
        }

        public override bool ShouldSerializeValue(object component)
        {
            return false;
        }

        public override object GetValue(object component)
        {
            return ((ElementSettingProperty)component)[Name];
        }

        public override void ResetValue(object component)
        {

        }

        public override void SetValue(object component, object value)
        {
            ((ElementSettingProperty)component)[Name] = value;
        }

        public override Type ComponentType
        {
            get { return typeof(ElementSettingProperty); }
        }

        public override Type PropertyType
        {
            get
            {
                return Parent[Name].GetType();
            }
        }

        public override bool IsReadOnly
        {
            get { return false; }
        }
    }
}