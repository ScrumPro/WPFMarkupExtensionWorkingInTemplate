﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Windows.Input;
using DevExpress.Mvvm.Native;
using WPFSearch.Core;
using WPFSearch.Helpers;

namespace WPFSearch
{
    public class MainViewModel : IBaseViewModel, IElementSave
    {
        public ObservableCollection<ElementPreferenceSetting> ElementPreferenceSettings { get; set; }

        private ICommand _openPropertyEditorViewCommand;
        public ICommand OpenPropertyEditorViewCommand
        {
            get
            {
                return _openPropertyEditorViewCommand ?? (_openPropertyEditorViewCommand = new RelayCommand(OpenPropertyEditorView));
            }
        }

        private void OpenPropertyEditorView(object sender)
        {
            var propertyEditorView = new PropertyEditorView();
            var propertyEditorViewModel = propertyEditorView.DataContext as PropertyEditorViewModel;
            if (propertyEditorViewModel == null) return;

            var sourceElement = sender as Control;
            if(sourceElement == null) return;
            
            var properties = new Dictionary<ElementSettingPropertyName, object>();
            sourceElement.Tag.ToString().Split(',').ForEach(property =>
            {
                var propertyName = (ElementSettingPropertyName)Enum.Parse(typeof(ElementSettingPropertyName), property);
                var propertyValue = GetProperty(sender, property);
                properties.Add(propertyName, propertyValue);    
            });
            

            propertyEditorViewModel.InitElementSettingProperties(properties);

            propertyEditorView.ShowDialog();

            var updatedProperties = propertyEditorViewModel.ElementSettingProperty;
            updatedProperties.ForEach(property =>
            {
                SetProperty(sender, property.Key.ToString(), property.Value);
            });
        }

        private object GetProperty(object sender, string propertyName)
        {
            if (sender == null) return null;

            var propertyValue = sender.GetType().GetProperty(propertyName).GetValue(sender, null);
            return propertyValue;
        }

        private void SetProperty(object sender, string propertyName, object propertyValue)
        {
            if (sender == null) return;

            sender.GetType().GetProperty(propertyName).SetValue(sender, propertyValue);
        }

        public MainViewModel()
        {
            ViewName = "MainView";

            ElementPreferenceSettings = new ObservableCollection<ElementPreferenceSetting>(
                new List<ElementPreferenceSetting>
                {
                    new ElementPreferenceSetting
                    {
                        ElementName = "RowOfInstruction",
                        PropertyName = PropertyName.GridRowHeight,
                        PropertyValue = 300
                    },

                    new ElementPreferenceSetting
                    {
                        ElementName = "ColOfInstruction",
                        PropertyName = PropertyName.GridColWidth,
                        PropertyValue = 300
                    },

                    new ElementPreferenceSetting
                    {
                        ElementName = "Text01",
                        PropertyName = PropertyName.Width,
                        PropertyValue = 60.00
                    },
                    new ElementPreferenceSetting
                    {
                        ElementName = "Text01",
                        PropertyName = PropertyName.Height,
                        PropertyValue = 100.00
                    },
                    new ElementPreferenceSetting
                    {
                        ElementName = "Text01",
                        PropertyName = PropertyName.Foreground,
                        PropertyValue = "#FF0000"
                    },
                    new ElementPreferenceSetting
                    {
                        ElementName = "Text01",
                        PropertyName = PropertyName.Background,
                        PropertyValue = "#FF00FF"
                    },

                    new ElementPreferenceSetting
                    {
                        ElementName = "Text02",
                        PropertyName = PropertyName.Foreground,
                        PropertyValue = "#0000FF"
                    },
                });
        }

        public string ViewName { get; private set; }
    }
}
