﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using WPFSearch.Annotations;

namespace WPFSearch.Helpers
{
    public class NotificationObject : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public bool SetProperty<T>(ref T property, T value, [CallerMemberName] string propertyName = null, IEqualityComparer<T> comparer = null)
        {
            comparer = comparer ?? EqualityComparer<T>.Default;
            var result = !comparer.Equals(property, value);

            if (result)
            {
                property = value;
                OnPropertyChanged(propertyName);
            }

            return result;
        }
    }
}
