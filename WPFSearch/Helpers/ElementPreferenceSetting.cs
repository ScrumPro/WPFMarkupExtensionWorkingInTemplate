﻿namespace WPFSearch.Helpers
{
    public enum PropertyName
    {
        Height, Width, GridRowHeight, GridColWidth, Foreground, Background
    }

    public class ElementPreferenceSetting
    {
        public string ElementName { get; set; }

        public PropertyName PropertyName { get; set; }

        public object PropertyValue { get; set; }
    }
}
